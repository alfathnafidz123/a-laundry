@extends('layouts.appowner')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard Kasir</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <br>
                    <div align="center">
                    <a href="{{url('/laporan/cetak')}}" class="btn btn-success">Print Laporan</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
