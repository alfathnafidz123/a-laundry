@extends('layouts.header')

@section('content')
<body>
    <div class="container">
      <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
          <div class="card card-signin my-5">
            <div class="card-body">
            <h2 class="text-center"> A-laundry </h2>
              <h5 class="card-title text-center">{{ __('Login') }}</h5>
              <form class="form-signin" method="POST" action="{{ route('login') }}">
              @csrf
                <div class="form-label-group">
                  <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email address" name="email" value="{{ old('email') }}" required autofocus>
                  <label for="email">{{ __('E-Mail Address') }}</label>
                  @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
  
                <div class="form-label-group">
                  <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>
                  <label for="password">{{ __('Password') }}</label>
                  @if ($errors->has('password'))
                      <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
                </div>
  
                <div class="custom-control custom-checkbox mb-3">
                  <input type="checkbox" class="custom-control-input" id="customCheck1" {{ old('remember') ? 'checked' : '' }}>
                  <label class="custom-control-label" for="customCheck1">{{ __('Remember Me') }}</label>
                </div>
                <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">{{ __('Login') }}</button>
            
                    <a class="btn btn-lg btn-danger btn-block text-uppercase" href="/register">Register
                    </a>
                    
                
                <hr class="my-4">
                @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                       {{ __('Forgot Your Password?') }}
                    </a>
                @endif
                
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
@endsection