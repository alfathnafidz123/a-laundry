
<style>
        body{
            padding: 0;
            margin: 0;
            align: center;
        }
        .page{
            max-width: 80em;
            margin: 0 auto;
        }
        table th,
        table td{
            text-align: left;
            border: 1px solid black; 
        }
        table.layout{
            width: 100%;
            border-collapse: collapse;
        }
        table.display{
            margin: 1em 0;
        }
        table.display th,
        table.display td{
            border: 1px solid #B3BFAA;
            padding: .5em 1em;
        }
​
        table.display th{ background: #D5E0CC; }
        table.display td{ background: #fff; }
​
        table.responsive-table{
            box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
        }
​
        .listcust {
            margin: 0;
            padding: 0;
            list-style: none;
            display:table;
            border-spacing: 10px;
            border-collapse: separate;
            list-style-type: none;
        }
​
        .customer {
            padding-left: 600px;
        }
    </style>
<div class="container">
    <div class="card p-5">
        <br>
        <br>
        
        <h3><center>Laporan Transaksi</center></h3><br>
        <br>
        <table align='center'>
                <thead>
                  <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Id</th>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Member</th>
                    <th class="text-center">Paket</th>
                    <th class="text-center">Berat (KG)</th>
                    <th class="text-center">Biaya Tambahan</th>
                    <th class="text-center">Harga Total</th>
                    <th class="text-center">Status Pesanan</th>
                    <th class="text-center">Status Pembayaran</th>
                    <th class="text-center">Tanggal Bayar</th>
                  </tr>
                </thead>
                <tbody>
                    @php
                     $i = 1; 
                    @endphp
                    @foreach ($transaksi as $item)
                        <tr>
                            <th scope="row"><?= $i++; ?></th>
                            <td class="text-center">{{$item->id_transaksi}}</td>
                            <td class="text-center">{{$item->tanggal}}</td>
                            <td class="text-center">{{$item->nama}}</td>
                            <td class="text-center">{{$item->nama_paket}}</td>
                            <td class="text-center">{{$item->berat}}</td>
                            <td class="text-center">{{$item->biaya_tambahan}}</td>
                            <td class="text-center">{{$item->harga_total}}</td>
                            <td class="text-center">{{$item->nama_status_pesanan}}</td>
                            <td class="text-center">{{$item->nama_status_pembayaran}}</td>
                            <td class="text-center">{{$item->tanggal_bayar}}</td>
                        </tr>
                    @endforeach
                </tbody>
        </table>
    </div>
</div>