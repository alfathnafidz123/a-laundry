@extends('layouts.appkasir')

@section('content')
<!doctype html>
<html>
    <head>
        
        <title>Member</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    Manage Member 
                </div>
                <div class="card-body">
                    <br/>
                    
                    <form method="post" action="{{ url('/kasir/datamember' ,@$member->id_member) }}">
 
                        {{ csrf_field() }}
 
                        <div class="form-group">
                            <label>Nama</label>
                            <input class="form-control" type="text" name="nama" value="{{ old('nama', @$member->nama) }}" placeholder="Nama">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <input class="form-control" type="text" name="alamat" value="{{ old('alamat', @$member->alamat) }}" placeholder="Alamat">
                        </div>
                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <input class="form-control" type="text" name="jenkel" value="{{ old('jenkel', @$member->jenkel) }}" placeholder="L/P" >
                        </div>
                        <div class="form-group">
                            <label>No Telp</label>
                            <input class="form-control" type="text" name="no_tlp" value="{{ old('no_tlp', @$member->no_tlp) }}" placeholder="No. Telepon">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
 
                    </form>
 
                </div>
            </div>
        </div>
    </body>
</html>
@endsection