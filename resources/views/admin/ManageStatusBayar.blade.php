
@extends('layouts.app')

@section('content')
<!doctype html>
<html>
    <head>
        
        <title>Transaksi</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    Manage Status Pembayaran 
                </div>
                <div class="card-body">
                    <br/>
                    
                    <form method="post" action="{{url('/admin/datastatusbayar' ,@$status_pembayaran->id_status_pembayaran) }}">
 
                        {{ csrf_field() }}
 
                        <div class="form-group">
                            <label>id</label>
                            <input class="form-control" type="text" name="id_status_pembayaran" value="{{ old('id_status_pembayaran', @$status_pembayaran->id_status_pembayaran) }}" placeholder="id">
                        </div>
                        <div class="form-group">
                            <label>Nama Status</label>
                            <input class="form-control" type="text" name="nama_status_pembayaran" value="{{ old('nama_status_pembayaran', @$status_pembayaran->nama_status_pembayaran) }}" placeholder="Nama Status">
                        </div>
                        <div class="form-group">
                            <label>Urutan</label>
                            <input class="form-control" type="text" name="urutan" value="{{ old('urutan', @$status_pembayaran->urutan) }}" placeholder="Urutan">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
 
                    </form>
 
                </div>
            </div>
        </div>
    </body>
</html>
@endsection