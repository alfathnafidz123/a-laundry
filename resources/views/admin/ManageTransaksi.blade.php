
@extends('layouts.app')

@section('content')
<!doctype html>
<html>
    <head>
        
        <title>Transaksi</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    Tambah Transaksi 
                </div>
                <div class="card-body">
                    <br/>
                    
                    <form method="post" action="{{ url('/admin/datatransaksi' ,@$transaksi->id_transaksi) }}">
 
                        {{ csrf_field() }}

                        <input type="hidden" name="tanggal"
                        value="<?php
                                $tgl=date('Y-m-d');
                                echo $tgl;
                                ?>">
 
                        <div class="form-group">
                            <label>Member</label>
                            <select class="form-control" name="member">
                                @foreach($c_member as $row)
                                    <option class="form-control" value="{{ old('member' ,@$row->id_member) }}">{{ @$row->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Paket</label>
                            <select class="form-control" name="paket">
                                @foreach($c_paket as $row)
                                    <option class="form-control" value="{{ old('paket' ,@$row->id_paket) }}">{{ @$row->nama_paket }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Berat</label>
                            <input class="form-control" type="text" name="berat" value=" {{ old('berat', @$transaksi->berat) }} " placeholder="(Kg)">
                        </div>
                        <div class="form-group">
                            <label>Biaya Tambahan</label>
                            <input class="form-control" type="text" name="biaya_tambahan" value=" {{ old('biaya_tambahan', @$transaksi->biaya_tambahan) }} " placeholder="Rp">
                        </div>
                        <div class="form-group">
                            <label>Status Pesanan</label>
                            <select class="form-control" name="status_pesanan">
                                @foreach($c_status_pesanan as $row)
                                    <option class="form-control" value="{{ old('status_pesanan' ,@$row->id_status_pesanan) }}">{{ @$row->nama_status_pesanan }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Status Pembayaran</label>
                            <select class="form-control" name="status_pembayaran">
                                @foreach($c_status_pembayaran as $row)
                                    <option class="form-control" value="{{ old('status_pembayaran' ,@$row->id_status_pembayaran) }}">{{ @$row->nama_status_pembayaran }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" name="tanggal_bayar"
                            value="<?php
                                    $tgl=date('Y-m-d');
                                    echo $tgl;
                                    ?>">
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
 
                    </form>
 
                </div>
            </div>
        </div>
    </body>
</html>
@endsection