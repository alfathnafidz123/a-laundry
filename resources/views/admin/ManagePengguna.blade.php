@extends('layouts.app')

@section('content')
<!doctype html>
<html>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    Manage Pengguna 
                </div>
                <div class="card-body">
                    <br/>
                    
                    <form method="post" action="{{ url('/admin/datapengguna' ,@$users->id) }}">
 
                        {{ csrf_field() }}
                        
                        <div class="form-group">
                            <label>Sebagai</label>
                            <select class="form-control" name="role">
                                    <option class="form-control" value="1"> Admin</option>
                                    <option class="form-control" value="2"> Kasir</option>
                                    <option class="form-control" value="3"> Owner</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama</label>
                            <input class="form-control" type="text" name="name" value="{{ old('name', @$users->name) }}" placeholder="Nama">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input class="form-control" type="text" name="email" value="{{ old('email', @$users->email) }}" placeholder="Nama">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <input class="form-control" type="text" name="alamat" value="{{ old('alamat', @$users->alamat) }}" placeholder="Alamat">
                        </div>
                        <div class="form-group">
                            <label>No Telp</label>
                            <input class="form-control" type="text" name="no_tlp" value="{{ old('no_tlp', @$users->no_tlp) }}" placeholder="No. Telepon">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
 
                    </form>
 
                </div>
            </div>
        </div>
    </body>
</html>
@endsection