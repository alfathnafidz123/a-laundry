@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card p-5">
    <h3><center>Data Pengguna</center></h3><br>
        <br>
        <table id="datatable" class="cell-border compact stripe col-12">
                <thead>
                  <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Id</th>
                    <th class="text-center">Role</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center">E-mail</th>
                    <th class="text-center">Alamat</th>
                    <th class="text-center">No. Telepon</th>
                    <th class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @php
                     $i = 1; 
                    @endphp
                    @foreach ($users as $item)
                        <tr>
                            <th scope="row"><?= $i++; ?></th>
                            <td class="text-center">{{$item->id}}</td>
                            <td class="text-center">{{$item->role}}</td>
                            <td class="text-center">{{$item->name}}</td>
                            <td class="text-center">{{$item->email}}</td>
                            <td class="text-center">{{$item->alamat}}</td>
                            <td class="text-center">{{$item->no_tlp}}</td>
                            <td class="text-center">
                        
                                <a href="{{ url('admin/datapengguna/' . $item->id )}}" class="btn btn-primary">Edit</a>
                                
                                
                                <a href="{{ url('admin/datapengguna/' . $item->id .'/delete')}}" class="btn btn-danger">Delete</a>
                                
                            </td>
                        </tr>
                    @endforeach
                </tbody>
        </table>
    </div>
</div>
@endsection
