
@extends('layouts.app')

@section('content')
<!doctype html>
<html>
    <head>
        
        <title>Outlet</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    Manage Outlet 
                </div>
                <div class="card-body">
                    <br/>
                    
                    <form method="post" action="{{ url('/admin/dataoutlet' ,@$outlet->id_outlet) }}">
 
                        {{ csrf_field() }}
 
                        <div class="form-group">
                            <label>Nama</label>
                            <input class="form-control" type="text" name="nama" value="{{ old('nama', @$outlet->nama) }}" placeholder="Nama">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <input class="form-control" type="text" name="alamat" value="{{ old('alamat', @$outlet->alamat) }}" placeholder="Alamat">
                        </div>
                        <div class="form-group">
                            <label>No Telp</label>
                            <input class="form-control" type="text" name="no_tlp" value="{{ old('no_tlp', @$outlet->no_tlp) }}" placeholder="No. Telepon">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
 
                    </form>
 
                </div>
            </div>
        </div>
    </body>
</html>
@endsection