

@extends('layouts.app')

@section('content')
<!doctype html>
<html>
    <head>
        
        <title>Paket</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    Manage Paket 
                </div>
                <div class="card-body">
                    <br/>
                    
                    <form method="post" action="{{ url('/admin/datapaket' ,@$paket->id_paket) }}">
 
                        {{ csrf_field() }}
 
                        <div class="form-group">
                            <label>Outlet</label>
                            <select class="form-control" name="id_outlet">
                                @foreach($c_outlet as $row)
                                    <option class="form-control" value="{{ old('id_outlet' ,@$row->id_outlet) }}">{{ @$row->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Jenis Paket</label>
                            <input class="form-control" type="text" name="jenis" value="{{ old('jenis', @$paket->jenis) }}" placeholder="Jenis Paket">
                        </div>
                        <div class="form-group">
                            <label>Nama Paket</label>
                            <input class="form-control" type="text" name="nama_paket" value="{{ old('nama_paket', @$paket->nama_paket) }}" placeholder="Nama Paket" >
                        </div>
                        <div class="form-group">
                            <label>Harga</label>
                            <input class="form-control" type="text" name="harga" value="{{ old('harga', @$paket->harga) }}" placeholder="Harga (RP) / Kg">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
 
                    </form>
 
                </div>
            </div>
        </div>
    </body>
</html>
@endsection