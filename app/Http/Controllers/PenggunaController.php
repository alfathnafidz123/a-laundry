<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class PenggunaController extends Controller{
    public function showData()
    {
        $data['users'] = \DB::table('users')->get();
        return view("admin/datapengguna",$data);
    }


    public function editdata($id)
    {
        $users = \DB::table('users')->where('id', $id)->first();
    	return view("admin\ManagePengguna", compact('users'));
    }

    public function updatedata(Request $request, $id)
    {
        $users = \DB::table('users')->where('id',$id)->update([
			'name' => $request->name,
			'email' => $request->email,
            'alamat' => $request->alamat,
            'no_tlp' => $request->no_tlp,
            'role' => $request->role
		]);
    	return redirect("admin/datapengguna");
    }

    public function destroy(Request $request, $id)
    {
        $result = \DB::table('users')->where('id', $id)->delete();

        if($result) return redirect()->back()->with('success', 'Data Berhasil Dihapus');
        else return view('datamember')->with('error', 'Data Gagal Dihapus');
    }

}