<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paket;
use App\Models\Status_pesanan;
use App\Models\Status_pembayaran;
use App\Models\Transaksi;
use PDF;

class LaporanController extends Controller{
    public function showData()
    {
        $data['transaksi'] = \DB::table('transaksi')
        ->join('member','member.id_member','=','transaksi.member')
        ->join('paket','paket.id_paket','=','transaksi.paket')
        ->join('status_pesanan','status_pesanan.id_status_pesanan','=','transaksi.status_pesanan')
        ->join('status_pembayaran','status_pembayaran.id_status_pembayaran','=','transaksi.status_pembayaran')
        ->get();
        return view("laporan/laporan", $data);
    }

    public function print_pdf()
    {
        $data['transaksi'] = \DB::table('transaksi')
        ->join('member','member.id_member','=','transaksi.member')
        ->join('paket','paket.id_paket','=','transaksi.paket')
        ->join('status_pesanan','status_pesanan.id_status_pesanan','=','transaksi.status_pesanan')
        ->join('status_pembayaran','status_pembayaran.id_status_pembayaran','=','transaksi.status_pembayaran')
        ->get();
        
        $pdf = PDF::loadview("laporan/laporan", $data);
        return $pdf->download('laporan-pdf.pdf');
    }

    public function count($transaksi)
    {
        $total = 0;
        foreach($subtotal as $item) {
            $subtotal = $transaksi->pluck('harga_total')->all();
            $total += $item;
        }
        return view('laporan/laporan',['total' => $total]);
    }
}