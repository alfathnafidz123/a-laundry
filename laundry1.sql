-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 16 Apr 2020 pada 09.40
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laundry1`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `member`
--

CREATE TABLE `member` (
  `id_member` int(11) NOT NULL,
  `nama` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenkel` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_tlp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `member`
--

INSERT INTO `member` (`id_member`, `nama`, `alamat`, `jenkel`, `no_tlp`, `created_at`, `updated_at`) VALUES
(4, 'Namlih Sirat', 'GBI', 'L', '08992291007', NULL, NULL),
(5, 'Aceng', 'Jauh Moal Ka Udag', 'L', '0899229918', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_03_11_031027_create_member_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 2),
(5, '2020_03_11_032322_create_paket_table', 2),
(6, '2020_03_11_060327_create_table_outlet', 3),
(7, '2020_03_11_060730_create_transaksi_paket', 4),
(8, '2020_03_11_062350_create_dettransaksi_table', 5),
(9, '2020_03_26_045029_create_transaksi_table', 6),
(10, '2020_03_26_051730_create_status_pesanan_table', 7),
(11, '2020_03_26_052122_create_status_pembayaran_table', 7),
(12, '2020_03_26_053805_create_transaksi_table', 8),
(13, '2020_03_26_054519_create_status_pemesanan_table', 9),
(14, '2020_03_26_054607_create_status_pembayaran_table', 9),
(15, '2020_03_26_054814_create_transaksi_table', 10),
(16, '2020_03_26_065547_create_status_pesanan_table', 11),
(17, '2020_03_26_065609_create_status_pembayaran_table', 11),
(18, '2020_03_26_070245_create_transaksi_table', 12),
(19, '2020_03_26_073123_create_status_pesanan_table', 13),
(20, '2020_03_26_073133_create_status_pembayaran_table', 13),
(21, '2020_03_26_073440_create_transaksi_table', 14),
(22, '2020_03_27_021812_alter_table_transaksi', 15),
(23, '2020_03_27_022704_alter_table_transaksi', 16),
(24, '2020_04_14_081204_alter_table_users', 17),
(25, '2020_04_14_081454_alter_table_users1', 18),
(26, '2020_04_14_081854_alter_table_users', 19),
(27, '2020_04_14_092729_alter_table_member', 20);

-- --------------------------------------------------------

--
-- Struktur dari tabel `paket`
--

CREATE TABLE `paket` (
  `id_paket` int(11) NOT NULL,
  `id_outlet` int(11) NOT NULL,
  `jenis` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_paket` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `paket`
--

INSERT INTO `paket` (`id_paket`, `id_outlet`, `jenis`, `nama_paket`, `harga`, `created_at`, `updated_at`) VALUES
(1, 1, 'Pakaian', 'Baju', 10000, NULL, NULL),
(3, 1, 'Kiloan', 'Kiloan', 15000, NULL, NULL),
(4, 1, 'Alat Tidur', 'Selimut', 30000, NULL, NULL),
(5, 2, 'Seragam', '1 pasang seragam', 20000, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_pembayaran`
--

CREATE TABLE `status_pembayaran` (
  `id_status_pembayaran` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_status_pembayaran` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `urutan` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `status_pembayaran`
--

INSERT INTO `status_pembayaran` (`id_status_pembayaran`, `nama_status_pembayaran`, `urutan`, `created_at`, `updated_at`) VALUES
('1', 'Belum Bayar', '1', NULL, NULL),
('2', 'Lunas', '2', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_pesanan`
--

CREATE TABLE `status_pesanan` (
  `id_status_pesanan` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_status_pesanan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `urutan` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `status_pesanan`
--

INSERT INTO `status_pesanan` (`id_status_pesanan`, `nama_status_pesanan`, `urutan`, `created_at`, `updated_at`) VALUES
('1', 'Menunggu Antrian', '1', NULL, NULL),
('2', 'Dalam Proses', '2', NULL, NULL),
('3', 'Selesai', '3', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_outlet`
--

CREATE TABLE `table_outlet` (
  `id_outlet` int(11) NOT NULL,
  `nama` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_tlp` int(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `table_outlet`
--

INSERT INTO `table_outlet` (`id_outlet`, `nama`, `alamat`, `no_tlp`, `created_at`, `updated_at`) VALUES
(1, 'A-Laundry', 'Saint-Petersburg', 851551677, NULL, NULL),
(2, 'Londri', 'Cembul', 899229199, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(10) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `member` int(11) NOT NULL,
  `paket` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `biaya_tambahan` int(11) NOT NULL,
  `harga_total` int(11) NOT NULL,
  `status_pesanan` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_pembayaran` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `tanggal`, `member`, `paket`, `berat`, `biaya_tambahan`, `harga_total`, `status_pesanan`, `status_pembayaran`, `tanggal_bayar`, `created_at`, `updated_at`) VALUES
(1, '2020-04-14', 4, 5, 10, 1000, 201000, '1', '2', '2020-04-14', '2020-04-14 02:30:17', '2020-04-14 02:30:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_tlp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `alamat`, `no_tlp`, `role`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(5, 'admin', 'admin1@mail.com', NULL, NULL, '1', NULL, '$2y$10$4J0b8XCRn6R4jNeUSJfkT.a12DJqma2rxAGksIjUQNpx.Sx9CKIjy', NULL, '2020-04-06 20:13:29', '2020-04-06 20:13:29'),
(15, 'Alfath Nafidz', 'alfath@gmail.com', 'Saint-Petersburg', '085155167711', '3', NULL, '$2y$10$ZmpmOYXUzbfomUvdgvsIqe1TJ7qRbP/GPP3zx2RFjHYgHRkM221wm', NULL, '2020-04-14 02:22:45', '2020-04-14 02:22:45'),
(16, 'user', 'prulmail@mail.id', 'Bandung', '085155167711', '2', NULL, '$2y$10$QDCWFb/reyrp/nu5sBY87ebHf3SolQNDwC5rbWXb/oG4LHjRksdPa', NULL, '2020-04-15 21:24:36', '2020-04-15 21:24:36');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `paket`
--
ALTER TABLE `paket`
  ADD PRIMARY KEY (`id_paket`),
  ADD KEY `id_outlet` (`id_outlet`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `status_pembayaran`
--
ALTER TABLE `status_pembayaran`
  ADD PRIMARY KEY (`id_status_pembayaran`);

--
-- Indeks untuk tabel `status_pesanan`
--
ALTER TABLE `status_pesanan`
  ADD PRIMARY KEY (`id_status_pesanan`);

--
-- Indeks untuk tabel `table_outlet`
--
ALTER TABLE `table_outlet`
  ADD PRIMARY KEY (`id_outlet`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `transaksi_member_foreign` (`member`),
  ADD KEY `transaksi_paket_foreign` (`paket`),
  ADD KEY `transaksi_status_pesanan_foreign` (`status_pesanan`),
  ADD KEY `transaksi_status_pembayaran_foreign` (`status_pembayaran`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `member`
--
ALTER TABLE `member`
  MODIFY `id_member` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `paket`
--
ALTER TABLE `paket`
  MODIFY `id_paket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `table_outlet`
--
ALTER TABLE `table_outlet`
  MODIFY `id_outlet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `paket`
--
ALTER TABLE `paket`
  ADD CONSTRAINT `paket_ibfk_1` FOREIGN KEY (`id_outlet`) REFERENCES `table_outlet` (`id_outlet`);

--
-- Ketidakleluasaan untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_member_foreign` FOREIGN KEY (`member`) REFERENCES `member` (`id_member`),
  ADD CONSTRAINT `transaksi_paket_foreign` FOREIGN KEY (`paket`) REFERENCES `paket` (`id_paket`),
  ADD CONSTRAINT `transaksi_status_pembayaran_foreign` FOREIGN KEY (`status_pembayaran`) REFERENCES `status_pembayaran` (`id_status_pembayaran`),
  ADD CONSTRAINT `transaksi_status_pesanan_foreign` FOREIGN KEY (`status_pesanan`) REFERENCES `status_pesanan` (`id_status_pesanan`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
